<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id_admin
 * @property integer $id_level
 * @property string $username
 * @property string $password
 * @property string $nama_lengkap
 * @property string $keterangan
 * @property string $email
 * @property string $join_date
 * @property string $avatar
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Level $idLevel
 */
class User extends CActiveRecord
{
	public $password2;
	public $saltpassword;
	//public $verifyCode;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_level, username, password', 'required'),
			array('id_level', 'numerical', 'integerOnly'=>true),
    		array('username', 'match', 'pattern'=>'/^[A-Za-z]+$/', 'message'=>'Nama Pengguna Harus Berupa Huruf!'),
			array('username, keterangan, email', 'length', 'max'=>50),
			array('password, nama_lengkap', 'length', 'max'=>100),
            array('username','unique'),
			array('username', 'filter', 'filter'=>'strtolower'),
			array('email','unique'),
			array('password, saltPassword, email', 'length', 'max'=>50),
			array('avatar','file', 'types'=>'gif,png,jpg'),
			//array('status', 'length', 'max'=>1),
			array('join_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_admin, id_level, username, password,saltPassword, nama_lengkap, keterangan, email, join_date, avatar, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'level' => array(self::BELONGS_TO, 'Level', 'id_level'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_admin' => 'Id Admin',
			'id_level' => 'Level',
			'username' => 'Username',
			'password' => 'Password',
			'password2' => 'Password2',
			'nama_lengkap' => 'Nama Lengkap',
			'keterangan' => 'Keterangan',
			'email' => 'Email',
			'join_date' => 'Join Date',
			'avatar' => 'Avatar',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_admin',$this->id_admin);
		$criteria->compare('id_level',$this->id_level);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('saltPassword',$this->saltPassword,true);
		$criteria->compare('nama_lengkap',$this->nama_lengkap,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('join_date',$this->join_date,true);
		$criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	//validasi login.
	public function validatePassword($password)
	{
		return $this->hashPassword($password,$this->saltPassword)===$this->password;
	}
		public function hashPassword($password,$salt)
	{
		return md5($salt.$password);
	}
	
	public function generateSalt()
	{
		return uniqid('',true);
	}
	
	public function status($ii)
	{
		if($ii==0)
			return 'Belum Aktif / Banned';
		else 
			return 'Aktif';
	}
}