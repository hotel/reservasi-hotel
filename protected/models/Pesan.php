<?php

/**
 * This is the model class for table "pesan".
 *
 * The followings are the available columns in table 'pesan':
 * @property integer $id_pesan
 * @property string $check_in
 * @property string $check_out
 * @property integer $id_class
 * @property string $harga
 * @property integer $id_kamar
 * @property string $total
 * @property string $status
 */
class Pesan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pesan the static model class
	 */
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pesan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	
	
			public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('total', 'default', 'value'=>date('Ys203'), 'on'=>'insert'),
		
			array('nama_pemesan,check_in, check_out,email, id_class, id_kamar', 'required'),
			array('check_in','compare','compareAttribute'=>'check_out','operator'=>'<', 'message'=>'Tanggal harus lebih besar dari check_in'),
			array('id_class, id_kamar,telepon', 'numerical', 'integerOnly'=>true),
			array('harga,lama_inap', 'length', 'max'=>15),
			/* check email valid*/
			//array('email','email','checkMX'=>true),	
			//array('email,telepon','unique'),
			array('nama_pemesan', 'match','pattern'=>'/^([a-zA-Z`._])+/','message'=>'Tidak boleh berupa angka dan karakter spesial'),
			array('status', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_pesan,nama_pemesan,email,telepon,check_in, check_out,lama_inap, id_class, harga, id_kamar, total, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'class'=>array(self::BELONGS_TO,'type','id_class'),
		'kamar'=>array(self::BELONGS_TO,'Kamar','id_kamar'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pesan' => 'Id Pesan',
			'nama_pemesan'=>'Nama Pemesan',
			'email' => 'E-Mail',
			'telepon' => 'Telepon',
			'check_in' => 'Check-In',
			'check_out' => 'Check-Out',
			'lama_inap' => 'Lama Menginap',
			'id_class' => 'Class Hotel',
			'harga' => 'Harga',
			'id_kamar' => 'Kamar',
			'total' => 'Total',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pesan',$this->id_pesan);
		$criteria->compare('nama_pemesan',$this->nama_pemesan,true);		
		$criteria->compare('email',$this->email,true);
		$criteria->compare('telepon',$this->telepon,true);
		$criteria->compare('check_in',$this->check_in,true);		
		$criteria->compare('check_out',$this->check_out,true);
		$criteria->compare('id_class',$this->id_class);
		$criteria->compare('lama_inap',$this->lama_inap);		
		$criteria->compare('harga',$this->harga,true);
		$criteria->compare('id_kamar',$this->id_kamar);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}