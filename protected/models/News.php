<?php

/**
 * This is the model class for table "{{news}}".
 *
 * The followings are the available columns in table '{{news}}':
 * @property integer $id
 * @property integer $category_id
 * @property string $title
 * @property string $slug
 * @property string $image
 * @property string $description
 * @property string $datenews
 */
class News extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dr_news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, title, description,published', 'required'),
			array('category_id', 'numerical', 'integerOnly'=>true),
			array('title, slug, image', 'length', 'max'=>100),
                        array('image', 'file', 'allowEmpty'=>true, 'types'=>'jpeg,jpg,gif,png'),
                        array('slug', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, category_id, title, slug, image, description, datenews', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'category' => array(self::BELONGS_TO, 'Category', 'category_id')
		);
	}
        
        public function scopes() {
            return array(
                'newss' => array(
                    'condition' => 'category_id = 2'
                ),
                'events' => array(
                    'condition' => 'category_id = 3'
                ),
                'newsorder' => array(
                    'order' => 'datenews DESC',
                    
                ),
                '_published' => array(
                    'condition' => 'published = 1'
                    
                ),
                
                'recent_news' => array(
                    'order' => 'id desc',
                    'limit' => 10
                ),
            );
        }

        /**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
			'title' => 'Title',
			'slug' => 'Slug',
			'image' => 'Image',
			'description' => 'Description',
			'datenews' => 'Datenews',
                        'published' => 'Publish',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('datenews',$this->datenews,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        protected function beforeSave() {
            if(parent::beforeSave()){
                $title = str_replace(array(' ','+','=','"',':'), '-', $this->title);
                $this->slug = strtolower($title);
                // $this->datecreated = date('Y-m-d H:i:s');
            }
            return true;
        }
}