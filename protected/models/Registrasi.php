<?php

/**
 * This is the model class for table "registrasi".
 *
 * The followings are the available columns in table 'registrasi':
 * @property integer $id
 * @property string $ktp
 * @property string $id_pesan
 * @property string $alamat
 * @property string $telepon
 * @property string $email
 * @property string $jk
 */
class Registrasi extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Registrasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'registrasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ktp,telepon, email', 'required'),
			array('telepon,ktp,email','unique','message'=>'{attribute}{value} sudah dipakai'),			
			array('ktp, email,telepon', 'length', 'max'=>30),
			array('telepon', 'numerical','integerOnly'=>true,'message'=>'{attribute} hanya dapat diisi dengan nomor'),
			array('jk', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, ktp, id_pesan, alamat, telepon, email, jk', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pesan'=>array(self::BELONGS_TO,'Pesan','id_pesan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ktp' => 'Ktp',
			'id_pesan' => 'Id Pesan',
			'alamat' => 'Alamat',
			'telepon' => 'Telepon',
			'email' => 'Email',
			'jk' => 'Jk',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ktp',$this->ktp,true);
		$criteria->compare('id_pesan',$this->id_pesan,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('telepon',$this->telepon,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('jk',$this->jk,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}