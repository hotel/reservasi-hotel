<?php

/**
 * This is the model class for table "kamar".
 *
 * The followings are the available columns in table 'kamar':
 * @property integer $id_kamar
 * @property integer $id_class
 * @property string $namakamar
 * @property string $keterangan
 * @property string $gambar
 */
class Kamar extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Kamar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kamar';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_class, namakamar', 'required'),
			array('id_class', 'numerical', 'integerOnly'=>true),
			array('namakamar', 'length', 'max'=>15),
            array('gambar', 'file', 'allowEmpty'=>true, 'types'=>'jpeg,jpg,gif,png'),
			array('keterangan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_kamar, id_class, namakamar, keterangan, gambar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'class'=>array(self::BELONGS_TO,'Type','id_class'),
		//'kamar'=>array(self::BELONG_TO,'Kamar','id_kamar'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_kamar' => 'Id Kamar',
			'id_class' => 'Id Class',
			'namakamar' => 'Namakamar',
			'keterangan' => 'Keterangan',
			'gambar' => 'Gambar',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_kamar',$this->id_kamar);
		$criteria->compare('id_class',$this->id_class);
		$criteria->compare('namakamar',$this->namakamar,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('gambar',$this->gambar,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}