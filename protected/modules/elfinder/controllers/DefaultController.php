<?php

class DefaultController extends Controller {

    public function actionIndex() {
        $this->layout = false;

        Yii::import('elfinder.vendors.*');
        require_once('elFinder.class.php');

        $opts = array(
            'root' => Yii::getPathOfAlias("webroot")."/images/",
            'URL' => Yii::app()->baseUrl . '/images/',
            'rootAlias' => 'Home',
        );
        $fm = new elFinder($opts);
        $fm->run();
    }

}