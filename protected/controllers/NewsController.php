<?php

class NewsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = 'column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				//'users'=>array('@'),
				'expression'=>'$user->getLevel()==2',				
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','create','update'),
				//'users'=>array('admin'),
				'expression'=>'$user->getLevel()==1',				
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new News;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['News']))
		{
			$model->attributes=$_POST['News'];
            $model->image = CUploadedFile::getInstance($model, 'image');
			if($model->validate()){
                            if(!empty($model->image)){
                                $model->image = $this->saveImage($model, $model->image);
                            }
                            $model->save();    
                            $this->redirect(array('view','id'=>$model->id));
                        }
				
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

        $oldimage = $model->image;
        $valimage = FALSE;
                
		if(isset($_POST['News']))
		{
			$model->attributes=$_POST['News'];
                        $model->image=CUploadedFile::getInstance($model, 'image');
                        if(empty ($model->image)){
                            $model->image = $oldimage;
                        } else {
                            $valimage = TRUE;
                        }
			if($model->validate()){
                            if($valimage){
                                $this->deleteImage($oldimage);
                                $model->image = $this->saveImage($model, $model->image);
                            }
                            $model->save();
                            $this->redirect(array('view','id'=>$model->id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        $model = $this->loadModel($id);
                        $this->deleteImage($model->image);
			$model->delete();
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		   $criteria = new CDbCriteria();
				$dataProvider=new CActiveDataProvider('News',array(
                    'criteria'=>$criteria,
                    'sort'=>array(
                        'defaultOrder'=>'datenews DESC',
                    )
                ));
			$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new News('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['News']))
			$model->attributes=$_GET['News'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=News::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='news-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function saveImage($model, $images) {
            $date = date('Y-m-d');
            $images = $date.'-'.str_replace(array(':','+','=','"',' '), '_', $images);
            $images = strtolower($images);
            $path = Yii::getPathOfAlias('webroot') . Yii::app()->params->folder_news_ori . $images;
            $paththumb = Yii::getPathOfAlias('webroot') . Yii::app()->params->folder_news_thumb . $images;
            $model->image->saveAs($path);

            $img = Yii::app()->simpleImage->load($path);
            $img->resizeToWidth(384);
            $img->resizeToHeight(259);
            
            
            $img->save($paththumb);
            return $images;
        }
		
        public function deleteImage($images) {
            $dpath = Yii::getPathOfAlias('webroot') . Yii::app()->params->folder_news_ori . $images;
            $dpaththumb = Yii::getPathOfAlias('webroot') . Yii::app()->params->folder_news_thumb . $images;
            
            if(is_file($dpath))
                unlink ($dpath);
            
            if(is_file($dpaththumb))
                unlink ($dpaththumb);
        }
}
