<?php

class PesanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','create','view','update','DeletePesan','Set','Kirimemail','Namaaction','selectKamar'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','Kirimemail','toggle','switch','qtoggle'),
				//'users'=>array('@'),
				'expression'=>'$user->getLevel()==2',				
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','create','update','Kirimemail','toggle','switch','qtoggle','UpdatePesan'),
				//'users'=>array('admin'),
				'expression'=>'$user->getLevel()==1',				
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	//Mengambil data kamar
	public function actionNamaaction()
	{
	  $model=new Kamar('search');
	  $model->unsetAttributes(); // clear any default values
	  if(isset($_GET['Kamar']))
		 $model->attributes=$_GET['Kamar'];
	   $this->renderPartial('getKamar',array(
		  'model'=>$model,
	   ),false,true);

	   Yii::app()->end();
	 }
	//Mengambil data kamar	 
	 public function actionselectKamar($id)
	 {
	   $satu='';
	   $dua='';

		$model=Kamar::model()->findByPk($id);
		$satu=$model->primaryKey;
		$dua=$model->id_kamar;
		echo CJSON::encode(array
		(
		   'satu'=>$satu,
		   'dua'=>$dua,
		));
		Yii::app()->end();
	 }
	
	//membuat edit data field 
	public function actionUpdatePesan()
	{
		$es = new EditableSaver('Pesan');
		try {
			$es->update();
		} catch(CException $e) {
			echo CJSON::encode(array('success' => false, 'msg' => $e->getMessage()));
			return;
		}
    echo CJSON::encode(array('success' => true));
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	
	public function actionCreate()
	{
		$model=new Pesan;
	
//		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		if(isset($_POST['Pesan']))
		{
			$model->attributes=$_POST['Pesan'];		
			
			//mencari selisih tanggal
			$tgl1=$model->check_in;
			$tgl2=$model->check_out;
			$tes=(strtotime($tgl2) - strtotime($tgl1))/  ( 60 * 60 * 24 );
			$harga=$model->class->harga;
			$lama=$model->lama_inap;
			$lama=$tes;
			$model->lama_inap=$lama." "."Hari";
			$hasil=$harga*$tes;
			$rupiah=number_format($hasil);
			$model->total="Rp.".$rupiah;
		/*    
		$email=$model->email;
		$nama =$model->nama_pemesan;
		$class = $model->class->class;
		$mailer=Yii::createComponent('application.extensions.mailer.EMailer');
     	$mailer->IsSMTP();
     	$mailer->IsHTML(true);
     	$mailer->SMTPAuth = true;
     	$mailer->SMTPSecure = "ssl";
     	$mailer->Host = "smtp.gmail.com";
     	$mailer->Port = 465;
     	$mailer->Username = "tatasutiadi@fellow.lpkia.ac.id";
     	$mailer->Password = '';
     	$mailer->From = "Tata Sutiadi";
     	$mailer->FromName = "Konfirmasi Reservasi Hotel";
     	$mailer->AddAddress($email);
     	$mailer->Subject = "Pemesanan Kamar Hotel";
     	$mailer->Body = "Terimakasih sudah mendaftar di website kami :D ".
						"<br />"."Atas Nama :"." ".$nama." "."Dengan memesan kamar yang bertype"." ".$class." "."Dengan Total Pembayaran"." ".":".$rupiah;
     	*/
		//if($mailer->Send()) 
     	//{
          //	echo "Message sent successfully!";
     	//}
			//if($model->save() && $mailer->Send())
			if($model->save())
			$this->redirect(array('view','id'=>$model->id_pesan));
		}
		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionSet(){
	//$connection=Yii::app()->db;
	//$sql="select * from Kurikulum"
	//$sql=$connection->createCommand($sql)->execute();	
	//$sql=
	$data=Type::model()->findAllByAttributes(array('idclass'=>$_POST["harga"]));
	foreach($data as $d){
		$value=$d->idclass;
		$name =$d->harga;
		echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
	
	}
	
	
	//$data=CHtml::listData($data,'Kode','kota');
    //foreach($data as $value=>$name){ echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);}
	}
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		//$model=$this->loadModel($id);
		$model=Pesan::model()->findByPk($id);
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		//$model->total=NULL;
		
		if(isset($_POST['Pesan']))
		{
		$model->nama_pemesan=$_POST['Pesan']['nama_pemesan'];
		$model->email=$_POST['Pesan']['email'];
		$model->telepon=$_POST['Pesan']['telepon'];		
		$model->check_in=$_POST['Pesan']['check_in'];		
		$model->check_out=$_POST['Pesan']['check_out'];
		$model->lama_inap=$_POST['Pesan']['lama_inap'];
		$model->id_class=$_POST['Pesan']['id_class'];
		$model->harga=$_POST['Pesan']['harga'];
		$model->id_kamar=$_POST['Pesan']['id_kamar'];
		$model->total=$_POST['Pesan']['total'];
		$model->status=$_POST['Pesan']['status'];		
		$tgl1=$model->check_in;
		$tgl2=$model->check_out;
		$tes=(strtotime($tgl2) - strtotime($tgl1))/  ( 60 * 60 * 24 );
		$harga=$model->class->harga;
		$lama=$model->lama_inap;
		$lama=$tes;
		$model->lama_inap=$lama." "."Hari";
		
		$hasil=$harga*$lama;
		$rupiah=number_format($hasil);
		$model->total="Rp.".$rupiah;
		//$model->total=$hasil;		
		//$model=$this->loadModel($id);
		//$model->attributes=$_POST['Pesan'];
			//$model->attributes=$_POST['pesan']['total']=NULL;
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_pesan));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	public function actionDeletePesan($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('create'));
	}
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Pesan');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
/*
	public function actionKirimemail()
	{
    	$mailer = Yii::createComponent('application.extensions.mailer.EMailer');
     	$mailer->IsSMTP();
     	$mailer->IsHTML(true);
     	$mailer->SMTPAuth = true;
     	$mailer->SMTPSecure = "ssl";
     	$mailer->Host = "smtp.gmail.com";
     	$mailer->Port = 465;
     	$mailer->Username = "tatasutiadi@fellow.lpkia.ac.id";
     	$mailer->Password = '17041993';
     	$mailer->From = "Tata Sutiadi";
     	$mailer->FromName = "Konfirmasi Reservasi Hotel";
     	$mailer->AddAddress("tatasutiadi@gmail.com");
     	$mailer->Subject = "Percobaan.";
     	$mailer->Body = "Terimakasih sudah mendaftar !";
     	if($mailer->Send()) 
     	{
          	echo "Message sent successfully!";
     	}
     	else 
     	{
          echo "Fail to send your message!";
     	}
	}
	*/
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Pesan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pesan']))
			$model->attributes=$_GET['Pesan'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Pesan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Pesan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actions(){
    return array(
            'toggle'=>'ext.jtogglecolumn.ToggleAction',
            'switch'=>'ext.jtogglecolumn.SwitchAction', // only if you need it
            'qtoggle'=>'ext.jtogglecolumn.QtoggleAction', // only if you need it
    );
}


	/**
	 * Performs the AJAX validation.
	 * @param Pesan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pesan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
