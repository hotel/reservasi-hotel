<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','toggle','switch','qtoggle'),
				//'users'=>array('admin'),
				'expression'=>'$user->getLevel()==1',				
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$pass=$model->password;
			$model->saltPassword=$model->generateSalt();
			$model->password=$model->hashPassword($pass,$model->saltPassword);			
			$model->avatar = CUploadedFile::getInstance($model, 'avatar');
			if($model->validate()){
                            if(!empty($model->avatar)){
                                $model->avatar = $this->saveImage($model, $model->avatar);
                            }
                            $model->save();    
                            //Yii::app()->user->setFlash('success', "Berhasil");
							$this->redirect(array('view','id'=>$model->id));
                        }
				
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

        $oldimage = $model->avatar;
        $valimage = FALSE;
                
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
                        $model->avatar=CUploadedFile::getInstance($model, 'avatar');
                        if(empty ($model->avatar)){
                            $model->avatar = $oldimage;
                        } else {
                            $valimage = TRUE;
                        }
			if($model->validate()){
                            if($valimage){
                                $this->deleteImage($oldimage);
                                $model->avatar = $this->saveImage($model, $model->avatar);
                            }
                            $model->save();
                            $this->redirect(array('view','id'=>$model->id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
            $model = $this->loadModel($id);
            $this->deleteImage($model->avatar);
			$model->delete();
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('User');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        public function saveImage($model, $images) {
            $date = date('Y-m-d');
            $images = $date.'-'.str_replace(array(':','+','=','"',' '), '_', $images);
            $images = strtolower($images);
            $path = Yii::getPathOfAlias('webroot') . Yii::app()->params->folder_user . $images;
            $paththumb = Yii::getPathOfAlias('webroot') . Yii::app()->params->folder_user . $images;
            $model->avatar->saveAs($path);

            $img = Yii::app()->simpleImage->load($path);
            $img->resizeToWidth(384);
            $img->resizeToHeight(259);
            
            
            $img->save($paththumb);
            return $images;
        }
		
        public function deleteImage($images) {
            $dpath = Yii::getPathOfAlias('webroot') . Yii::app()->params->folder_user . $images;
            $dpaththumb = Yii::getPathOfAlias('webroot') . Yii::app()->params->folder_user . $images;
            
            if(is_file($dpath))
                unlink ($dpath);
            
            if(is_file($dpaththumb))
                unlink ($dpaththumb);
        }	
	public function actions(){
    return array(
            'toggle'=>'ext.jtogglecolumn.ToggleAction',
            'switch'=>'ext.jtogglecolumn.SwitchAction', // only if you need it
            'qtoggle'=>'ext.jtogglecolumn.QtoggleAction', // only if you need it
    );
}
}
