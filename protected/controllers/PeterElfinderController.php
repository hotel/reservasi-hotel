<?php
class PeterElfinderController extends Controller
{

public function actions()
{
return array(
'connector' => array(
'class' => 'ext.elFinder.ElFinderConnectorAction',
'settings' => array(
'root' => Yii::getPathOfAlias('webroot') . '/images/',
'URL' => Yii::app()->baseUrl . '/images/',
'rootAlias' => 'Home',
'mimeDetect' => 'none'
)
),
);
}

public function actionIndex() {

$this->render('index');
}
}
