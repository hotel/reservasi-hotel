<?php Yii::app()->bootstrap->register(); ?>
<?php
$this->breadcrumbs=array(
	'News'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Data Berita <span class="badge badge-success pull-right"> + </span>','url'=>array('index'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data Berita <span class="badge badge-success pull-right"> + </span>','url'=>array('create'),'visible'=>!yii::app()->user->isGuest),
);
?>
<h2><center>Berita Hotel</center></h2>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'news-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
      		'header'=>'No',
        	'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
        ),
		//'id',
		//'category.category',
		array(
		'name'=>'category_id',
		'filter'=>CHtml::activeDropdownlist($model,'category_id',CHtml::listdata(
			Category::model()->findAll('parent=1'),"id","category"),
			array(
				'empty'=>''
			)
		),
		'value'=>function($data){
			return $data->category->category;
		}
		),
		'title',
		//'slug',
		'image',
		'description:html',
		
		'datenews',
		//'published',
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
