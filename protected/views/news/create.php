<?php
$this->breadcrumbs=array(
	'News'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Data Berita <span class="badge badge-success pull-right"> + </span>','url'=>array('index'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-cog"></i> Kelola Berita <span class="badge badge-success pull-right"> + </span>','url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>

<h1>Create News</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>