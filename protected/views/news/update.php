<?php
$this->breadcrumbs=array(
	'News'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Data Berita <span class="badge badge-success pull-right"> + </span>','url'=>array('index'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data Berita <span class="badge badge-success pull-right"> + </span>','url'=>array('create'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-search"></i> Tampil Data Berita <span class="badge badge-success pull-right"> + </span>','url'=>array('view','id'=>$model->id),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-cog"></i> Kelola Berita <span class="badge badge-success pull-right"> + </span>','url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>

<h1>Update News <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>