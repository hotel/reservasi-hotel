<?php
$this->breadcrumbs=array(
	'News'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Data Berita <span class="badge badge-success pull-right"> + </span>','url'=>array('index'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data Berita <span class="badge badge-success pull-right"> + </span>','url'=>array('create'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-pencil"></i> Ubah Data Berita <span class="badge badge-success pull-right"> + </span>','url'=>array('update','id'=>$model->id),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-trash"></i> Hapus Berita <span class="badge badge-success pull-right"> + </span>','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=>Yii::app()->user->getLevel()==1),
	array('label'=>'<i class="icon icon-cog"></i> Kelola Berita <span class="badge badge-success pull-right"> + </span>','url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>
<?php/*
<h1>View News #<?php echo $model->id; ?></h1>
*/?>
<?php /*$this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'category_id',
		'title',
		'slug',
		array(
                    'name' => 'image',
                    'type' => 'html',
                    'value' => !empty($model->image) ? CHtml::image(Yii::app()->request->baseUrl . Yii::app()->params->folder_news_thumb . $model->image) : 'no image',
                ),
		'description:html',
		'datenews',
		'published',
	),
));*/ ?>

<h2><?php echo $model->title; ?></h2>
<b><i>Dibuat Pada : <?php echo $model->datenews; ?></i></b><br/>
<b>Oleh : </b><?php echo "Admin";//$model->webnews_created_by; ?>
<br/>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		array(
                    'name' => '',
                    'type' => 'html',
                    'value' => !empty($model->image) ? CHtml::image(Yii::app()->request->baseUrl . Yii::app()->params->folder_news_thumb . $model->image) : 'no image',
                ),
	),
));?>
<?php
$this->widget('bootstrap.widgets.TbBox', array(
    'title' =>'Deskripsi-Berita',
	'headerIcon' => 'icon-home',
	'content' =>$model->description,
));
?>
<div class="form-actions">
	<h2>Komen Berita</h2>
</div>
<?php
$this->widget('application.extensions.fb-comment.FBComment', array(
  'url' => 'www.facebook.com/myhotel',
  'posts' => 20,
  'width' => 370,
));?>