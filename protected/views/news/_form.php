<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'news-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'category_id'); ?>
		<?php echo $form->dropDownList($model,'category_id', CHtml::listData(Category::model()->findAll('parent=1'), 'id', 'category'),
                        array('empty' => 'choose : ')); ?>
		<?php echo $form->error($model,'category_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'image'); ?>
		<?php echo $form->fileField($model,'image'); ?>
		<?php echo $form->error($model,'image'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?><br /><br />
		<?php
                        $this->widget('application.extensions.tinymce.ETinyMce', 
                            array(
								'model'=>$model,
								'attribute'=>'description',
								'editorTemplate'=>'full',
                                )
                        ); 
                ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
<?php /*
	<div class="row">
		<?php echo $form->labelEx($model,'datenews'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
                            array(
                                'language'=>'id',
                                'model'=>$model,
                                'attribute'=>'datenews',
                                'value'=>$model->datenews,
                                'options'=>array(
                                        'showAnim'=>'slide',
                                        'dateFormat'=>'yy-mm-dd'
                                ),
                                'htmlOptions'=>array(
                                        'style'=>'height:15px;',
                                ),
                    ));
                ?>
		<?php echo $form->error($model,'datenews'); ?>
	</div>*/?>
        <div class="row">
            <?php echo $form->labelEx($model, 'published'); ?>
            <?php echo $form->dropDownList($model, 'published', array(
                        0 => 'No',
                        1 => 'Yes'
                    ), array('empty' => 'choose : '));
            ?>
            <?php echo $form->error($model, 'published'); ?>
        </div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array('type'=>'primary', 'label'=>'Simpan','buttonType'=>'submit')); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array('type'=>'reset', 'label'=>'Batal','buttonType'=>'reset')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->