<?php
$this->breadcrumbs=array(
	'News',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data Berita <span class="badge badge-success pull-right"> + </span>','url'=>array('create'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-cog"></i> Kelola Berita <span class="badge badge-success pull-right"> + </span>','url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>
<center><img src="<?php echo Yii::app()->request->baseUrl;?>/images/news.jpg" width="100%"></center><br/>
<h2><img width="110px" src="<?php echo Yii::app()->request->baseUrl;?>/images/newspaper.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;News</h2>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));
 ?>