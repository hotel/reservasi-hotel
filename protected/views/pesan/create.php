<?php
/* @var $this PesanController */
/* @var $model Pesan */

$this->breadcrumbs=array(
	'Pesans'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Data Pemesan <span class="badge badge-success pull-right"> + </span>', 'url'=>array('index'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-cog"></i> Kelola Pemesan <span class="badge badge-success pull-right"> + </span>', 'url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>
<div class="form-actions">
	<h1>Reservasi Hotel</h1>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>