<?php
/* @var $this PesanController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pesans',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data Pemesan <span class="badge badge-success pull-right"> + </span>', 'url'=>array('create'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-cog"></i> Kelola Pemesan <span class="badge badge-success pull-right"> + </span>', 'url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>

<h1>Pesans</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
