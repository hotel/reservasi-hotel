<?php
/* @var $this PesanController */
/* @var $data Pesan */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_pemesan')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->nama_pemesan), array('view', 'id'=>$data->id_pesan)); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('telepon')); ?>:</b>
	<?php echo CHtml::encode($data->telepon); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('check_in')); ?>:</b>
	<?php echo CHtml::encode($data->check_in); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('check_out')); ?>:</b>
	<?php echo CHtml::encode($data->check_out); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_class')); ?>:</b>
	<?php echo CHtml::encode($data->class->class); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('harga')); ?>:</b>
	<?php echo CHtml::encode($data->class->harga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kamar')); ?>:</b>
	<?php echo CHtml::encode($data->id_kamar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total')); ?>:</b>
	<?php echo CHtml::encode($data->total); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	*/ ?>

</div>