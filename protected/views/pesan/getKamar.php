<?php
 $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
   'id' => 'bandsdialog3',
   'options' => array(
   'autoOpen' => true,
   'title' => 'Pilih Kamar',
   'modal' => true,
   'width' => '600',
   ))
 );
 $this->widget('bootstrap.widgets.TbGridView', array(
   'id'=>'kamar-grid',
   'dataProvider'=>$model->search(),
   'filter'=>$model,
   'columns'=>array(
/*
   array(
		'name'=>'id_class',
		'value'=>function($data){
			return $data->class->class;
			}
		),*/	
		'namakamar',
        array(
           'header'=>'Pilih',
           'type'=>'raw',
           'value'=>'CHtml::link("Pilih","",array(
               "onClick"=>CHtml::ajax(array(
               "url"=>Yii::app()->createUrl("Pesan/selectKamar",array("id"=>$data->primaryKey)),
               "dataType"=>"json",
               "success"=>"function(data){
                     $(\"#Pesan_id_kamar\").val(data.dua);
                     $(\"#id_kamar\").val(data.dua);
                     $(\"#bandsdialog3\").remove();
                }")
            ),"id"=>"child".$data->primaryKey,"style"=>"cursor:pointer;"))',
         ),
       ),
  ));
  $this->endWidget('zii.widgets.jui.CJuiDialog');
 ?>
 