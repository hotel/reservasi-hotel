<?php //Yii::app()->bootstrap->register(); ?>
<?php
/* @var $this PesanController */
/* @var $model Pesan */

$this->breadcrumbs=array(
	'Pesans'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Daftar Pemesaan <span class="badge badge-success pull-right"> + </span>', 'url'=>array('index'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data Pemesan <span class="badge badge-success pull-right"> + </span>', 'url'=>array('create'),'visible'=>!yii::app()->user->isGuest),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pesan-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Pesans</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'pesan-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_pesan',
		array(
      		'header'=>'No',
        	'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
        ),
		'nama_pemesan',
		'email',
		'check_in',
		//'check_out',
		'lama_inap',
	array(
		'name'=>'id_class',
		'filter'=>CHtml::activeDropdownlist($model,'id_class',CHtml::listdata(
			Type::model()->findAll(),"idclass","class"),
			array(
				'empty'=>''
			)
		),
		'value'=>function($data){
			return $data->class->class;
		}
		),
	array(
		'name'=>'harga',
		
		'value'=>function($data){
			return $data->class->harga;
		}
		),
		array(
		'name'=>'id_kamar',
		
		'value'=>function($data){
			return $data->kamar->namakamar;
		}
		),
		//'total',
		
	/*	array(
                                'class'=>'JToggleColumn',
								'header'=>'<b>Aksi </b>',
                                'name'=>'status', // boolean model attribute (tinyint(1) with values 0 or 1)
                                'htmlOptions'=>array('style'=>'text-align:center;min-width:60px;')
                ),*/
                array(
                                        'class'=>'JToggleColumn',
                                        'name'=>'status', // boolean model attribute (tinyint(1) with values 0 or 1)
                                        //'filter' => array('0' => 'Ditolak', '1' => 'Disetujui'), // filter
                                        //'action'=>'switch', // other action, default is 'toggle' action
                                        'checkedButtonLabel'=>'images/togle/1.png',  // Image,text-label or Html
                                        'uncheckedButtonLabel'=>'images/togle/0.png', // Image,text-label or Html
                                        'checkedButtonTitle'=>'Diterima', // tooltip
                                        'uncheckedButtonTitle'=>'Dipesan', // tooltip
                                        'labeltype'=>'image',// New Option - may be 'image','html' or 'text'
                                        'htmlOptions'=>array('style'=>'text-align:center;min-width:60px;')
                ),
		/*
		array(
           'class' => 'editable.EditableColumn',
           'name' => 'status',
           'headerHtmlOptions' => array('style' => 'width: 110px'),
           'editable' => array(    //editable section
                  'apply'      => '$data->status != 4', //can't edit deleted users
                  'url'        => $this->createUrl('Pesan/updatePesan'),
                  'placement'  => 'top',
              )               
        ),*/		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
	<?php
		if (!yii::app()->user->isGuest){
			echo "
			<div align='right' style='color:red'>
			Ketentuan Status Check=DITERIMA dan UnCheck=DIPESAN </div>";
		}
	?>