<?php
/* @var $this PesanController */
/* @var $model Pesan */
/*
$this->breadcrumbs=array(
	'Pesans'=>array('index'),
	$model->id_pesan,
);*/

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Data Pemesan <span class="badge badge-success pull-right"> + </span>', 'url'=>array('index'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data Pemesan <span class="badge badge-success pull-right"> + </span>', 'url'=>array('create'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-pencil"></i> Ubah Data Pemesan <span class="badge badge-success pull-right"> + </span>', 'url'=>array('update', 'id'=>$model->id_pesan),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-trash"></i> Hapus Data Pemesan <span class="badge badge-success pull-right"> + </span>', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_pesan),'confirm'=>'Are you sure you want to delete this item?'),'visible'=>Yii::app()->user->getLevel()==1),
	array('label'=>'<i class="icon icon-cog"></i> Kelola Pemesan <span class="badge badge-success pull-right"> + </span>', 'url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>
<div class="form-actions">
	<h1>Detail Pesan #<?php echo $model->nama_pemesan; ?></h1>
</div>
<div align='right'>
	<?php
		echo " <a href='index.php?r=pesan/update&id=".$model->id_pesan."'><img src='images/edit.png' title='Ubah Pesanan' /></a> ";
	?>
</div>	
<?php
//$datetime1 = date_create('2009-10-11');
//$datetime2 = date_create('2009-10-13');
//$date_diff=DateDiff("$datetime1","$datetime2");
//echo $date_diff;	 
//$days = (strtotime("2005-11-20") - strtotime("2005-11-26")) / (60 * 60 * 24);
//print $days; 
/*	$tgl1=$model->check_in;
	 $tgl2=$model->check_out;
	 
	 $tes=(strtotime($tgl2) - strtotime($tgl1))/  ( 60 * 60 * 24 );
	 $harga=$model->class->harga;
//	 $lama=$tes;
	$lama=$model->lama_inap;
	$lama=$tes;	
	$hasil=$harga*$tes;
	 
	 echo $hasil;
	 echo $lama."Hari";
	*/ ?>
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id_pesan',
		'nama_pemesan',
		'email',
		'telepon',
		'check_in',
		'check_out',
		'lama_inap',
		'class.class',
		'class.harga',
		'kamar.namakamar',
		'total',
		//'status',
	),
)); ?>

	<div class="form-actions">
		<div align='right'>
		<?php
			echo CHtml::link('Batal Pemesanan',array('deletePesan', 'id'=>$model->id_pesan),array('class'=>'btn btn btn-primary'));
			//array('delete', 'id'=>$model->id_pesan)
		?> 	
		<?php echo Chtml::link('Cetak',array('#'),array('class'=>'btn btn btn-primary'));?>
		</div>
	</div>
	