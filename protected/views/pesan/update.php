<?php
/* @var $this PesanController */
/* @var $model Pesan */

$this->breadcrumbs=array(
	'Pemesan'=>array('index'),
	$model->nama_pemesan=>array('view','id'=>$model->id_pesan),
	'Update',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Data Pemesan <span class="badge badge-success pull-right"> + </span>', 'url'=>array('index'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data Pemesan <span class="badge badge-success pull-right"> + </span>', 'url'=>array('create'),'visible'=>yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-search"></i> Tampil Data Pemesan <span class="badge badge-success pull-right"> + </span>', 'url'=>array('view', 'id'=>$model->id_pesan),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-cog"></i> Kelola Pemesan <span class="badge badge-success pull-right"> + </span>', 'url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>
<div class="form-actions">
<h1>Ubah Data Pemesan #<?php echo $model->nama_pemesan; ?></h1>
</div>
<?php echo $this->renderPartial('_form_update', array('model'=>$model)); ?>