<?php
/* @var $this PesanController */
/* @var $model Pesan */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pesan-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_pemesan'); ?>
		<?php echo $form->textField($model,'nama_pemesan',array('size'=>25,'maxlength'=>125)); ?>
		<?php echo $form->error($model,'nama_pemesan'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>25,'maxlength'=>125)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'telepon'); ?>
		<?php echo $form->textField($model,'telepon',array('size'=>25,'maxlength'=>125)); ?>
		<?php echo $form->error($model,'telepon'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'check_in'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
                            array(
                                'language'=>'id',
                                'model'=>$model,
                                'attribute'=>'check_in',
                                'value'=>$model->check_in,
                                'options'=>array(
                                        'showAnim'=>'slide',
                                        'dateFormat'=>'yy-mm-dd'
                                ),
                                'htmlOptions'=>array(
                                        'style'=>'height:15px;',
                                ),
                    ));
         ?>
		<?php echo $form->error($model,'check_in'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'check_out'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
                            array(
                                'language'=>'id',
                                'model'=>$model,
                                'attribute'=>'check_out',
                                'value'=>$model->check_out,
                                'options'=>array(
                                        'showAnim'=>'slide',
                                        'dateFormat'=>'yy-mm-dd'
                                ),
                                'htmlOptions'=>array(
                                        'style'=>'height:15px;',
                                ),
                    ));
         ?>
		<?php echo $form->error($model,'check_out'); ?>
	</div>

	<div class="row">
		<?php 
		echo $form->labelEx($model,'id_class');
		$list=array();
		foreach(Type::model()->findAll() as $data){
			$list[$data->idclass] ="$data->idclass  "."-  ".$data->class;
		}
				echo $form->dropDownList($model,'id_class',$list,
						array(
							"ajax"=>array(
								"type"=>"post",
								"url"=>CController::createUrl('Pesan/Set'),
								"data"=>'js:{harga:$(this).val()}',
								"update"=>'#Pesan_harga'
							),
							'prompt'=>'Pilih Class ::','style'=>'width:100;'
						)
					);
		?>
		<?php echo $form->error($model,'id_class'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'harga'); ?>
		<?php 
				echo $form->dropDownList($model,'harga',array()); 		

		?>
		<?php echo $form->error($model,'harga'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_kamar'); ?>
		<?php echo $form->textField($model,'id_kamar'); ?>
		<?php echo $form->error($model,'id_kamar'); ?>
	</div>


	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array('type'=>'primary', 'label'=>'Selesai','buttonType'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->