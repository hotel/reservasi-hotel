<?php
/* @var $this KamarController */
/* @var $model Kamar */

$this->breadcrumbs=array(
	'Kamars'=>array('index'),
	$model->id_kamar=>array('view','id'=>$model->id_kamar),
	'Update',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Data Kamar <span class="badge badge-success pull-right"> + </span>', 'url'=>array('index'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data Kamar <span class="badge badge-success pull-right"> + </span>', 'url'=>array('create'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-search"></i> Lihat Data Kamar <span class="badge badge-success pull-right"> + </span>', 'url'=>array('view', 'id'=>$model->id_kamar),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-cog"></i> Kelola Kamar <span class="badge badge-success pull-right"> + </span>', 'url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>

<h1>Update Kamar <?php echo $model->id_kamar; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>