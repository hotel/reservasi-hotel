<?php
/* @var $this KamarController */
/* @var $model Kamar */

$this->breadcrumbs=array(
	'Kamar'=>array('index'),
	$model->namakamar,
);

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Data Kamar <span class="badge badge-success pull-right"> + </span>', 'url'=>array('index'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data Kamar <span class="badge badge-success pull-right"> + </span>', 'url'=>array('create'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-pencil"></i> Ubah Data Kamar <span class="badge badge-success pull-right"> + </span>', 'url'=>array('update', 'id'=>$model->id_kamar),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-trash"></i> Hapus Data Kamar <span class="badge badge-success pull-right"> + </span>', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_kamar),'confirm'=>'Are you sure you want to delete this item?'),'visible'=>Yii::app()->user->getLevel()==1),
	array('label'=>'<i class="icon icon-cog"></i> Kelola Kamar <span class="badge badge-success pull-right"> + </span>', 'url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>
<?php/*
<h1>View Kamar #<?php echo $model->namakamar; ?></h1>
*/?>
<?php /*$this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_kamar',
		'class.class',
		'namakamar',
				array(
                    'name' => 'gambar',
                    'type' => 'html',
                    'value' => !empty($model->gambar) ? CHtml::image(Yii::app()->request->baseUrl . Yii::app()->params->folder . $model->gambar) : 'no image',
                ),
		'keterangan:html',
	),
)); */?>

<h2><?php echo $model->namakamar; ?></h2>
<br>
<center>
	<img src="<?php echo Yii::app()->request->baseUrl.'/Kamar/'.$model->gambar; ?>" width="600"   class="thumbnail" height="400" title="<?php echo $model->namakamar; ?>" /><br/>
</center>
<b>Upload By : </b><?php echo "Admin";//$model->nama; ?></b>
<br/><br/>
<b>Type Kamar : </b><?php echo $model->class->class; ?>
<br/>
<?php
$this->widget('bootstrap.widgets.TbBox', array(
    'title' =>'Deskripsi',
    'headerIcon' => 'icon-home',
    'content' =>$model->keterangan,
));
?>
<div class="form-actions">
	<h2>Komen Kamar</h2>
</div>
<?php
$this->widget('application.extensions.fb-comment.FBComment', array(
  'url' => 'www.facebook.com/sutiadifdfh',
  'posts' => 20,
  'width' => 370,
));?>
