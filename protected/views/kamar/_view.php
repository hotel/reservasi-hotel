<?php
/* @var $this KamarController */
/* @var $data Kamar */
?>

<div class="view">
<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kamar')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_kamar), array('view', 'id'=>$data->id_kamar)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_class')); ?>:</b>
	<?php echo CHtml::encode($data->class->class); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('namakamar')); ?>:</b>
	<?php echo CHtml::encode($data->namakamar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gambar')); ?>:</b>
	<?php echo CHtml::encode($data->gambar); ?>
	<br />
Yii::app()->params->folder_news_ori . $images
	*/
?>

    <table border="0" width="100%">
        <tr>
            <td rowspan="3" width="20%"><div class="pict"><img src="<?php echo Yii::app()->request->baseUrl.'/Kamar/'.$data->gambar; ?>" width="150" height="100" title="<?php echo $data->class->class; ?>" /></div></td>
            
            <td width="45%"><h4> <div class="blc"><?php echo CHtml::link(CHtml::encode($data->namakamar), array('view', 'id'=>$data->id_kamar,'kamar'=>$data->namakamar)); ?></div></h4><br/> Type Kamar : <?php echo CHtml::encode($data->class->class); ?> <br/> Harga Perhari : <?php echo CHtml::encode($data->class->harga); ?></td>
        </tr>
        <?php /*
		<tr>
            
            <td><?php echo "alamat";//CHtml::encode($data->alamat); ?>, <?php echo "kota";//CHtml::encode($data->kota); ?>, <?php echo "Prov";//CHtml::encode($data->provinsi); ?></td>
        </tr> */ ?>
    </table>
</div><hr />