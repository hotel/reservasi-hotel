<?php
/* @var $this KamarController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Kamars',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Kamar <span class="badge badge-success pull-right"> + </span>', 'url'=>array('create'),'visible'=>!Yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-cog"></i> Kelola Kamar <span class="badge badge-success pull-right"> + </span>', 'url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>

<h1>Kamars</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
