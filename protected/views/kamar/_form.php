<?php
/* @var $this KamarController */
/* @var $model Kamar */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'kamar-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_class'); ?>
		<?php echo $form->dropdownlist($model,'id_class',CHtml::listdata(Type::model()->findAll(),'idclass','class'),
		array('empty' => 'Pilih Jenis Kamar ::','style'=>'width:100;')); ?>
		<?php echo $form->error($model,'id_class'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'namakamar'); ?>
		<?php echo $form->textField($model,'namakamar',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'namakamar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gambar'); ?>
		<?php echo $form->fileField($model,'gambar'); ?>
		<?php echo $form->error($model,'gambar'); ?>
	</div>
	<br />
	<?php echo $form->labelEx($model,'Fasilitas'); ?>
	<? echo CHtml::checkBox('hobby',FaLSE, array('value'=>'LCD',)), '&nbsp;Tv LCD';  ?><br />
	<? echo CHtml::checkBox('hobby2',FALSE, array('value'=>'Wifi',)), '&nbsp;Wifi';  ?><br />
	<? echo CHtml::checkBox('hobby3',FALSE, array('value'=>'Sarapan',)), '&nbsp;Sarapan';  ?><br />
	<? echo CHtml::checkBox('hobby',FaLSE, array('value'=>'Pelayanan ++',)), '&nbsp;Pelayanan plus';  ?><br />
	<? echo CHtml::checkBox('hobby2',FALSE, array('value'=>'Kamar Mandi',)), '&nbsp;Kamar Mandi';  ?><br />
	<? echo CHtml::checkBox('hobby3',FALSE, array('value'=>'Makan Malam',)), '&nbsp;Makan Malam';  ?><br />  
	<br />
	<div class="row">
		<?php echo $form->labelEx($model,'keterangan'); ?>
		<?php
                        $this->widget('application.extensions.tinymce.ETinyMce', 
                            array(
								'model'=>$model,
								'attribute'=>'keterangan',
								'editorTemplate'=>'full',
                                )
                        ); 
         ?>
		<?php echo $form->error($model,'keterangan'); ?>
	</div>
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array('type'=>'primary', 'label'=>'Simpan','buttonType'=>'submit')); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array('type'=>'reset', 'label'=>'Batal','buttonType'=>'reset')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->