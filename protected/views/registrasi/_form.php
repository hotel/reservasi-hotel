<?php
/* @var $this RegistrasiController */
/* @var $model Registrasi */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'registrasi-form',
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'ktp'); ?>
		<?php echo $form->textField($model,'ktp',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'ktp'); ?>
	</div>
<?/*
	<div class="row">
		<?php echo $form->labelEx($model,'id_pesan'); ?>
		<?php
		$connection	= Yii::app()->db;
		$sql = "select id from registrasi order by DESC limit 1";
		$sql=$connection->createCommand($sql)->execute();
		echo $form->textField($model,'id_pesan'); ?>
		<?php echo $form->error($model,'id_pesan'); ?>
	</div>
*/?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_pesan'); ?>
		<?php echo $form->textField($model,'id_pesan',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'id_pesan'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'alamat'); ?>
		<?php echo $form->textArea($model,'alamat',array('rows'=>10, 'cols'=>90)); ?>
		<?php echo $form->error($model,'alamat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telepon'); ?>
		<?php echo $form->textField($model,'telepon',array('size'=>13,'maxlength'=>13)); ?>
		<?php echo $form->error($model,'telepon'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<div class="form-actions">
		<?php echo $form->labelEx($model,'jk'); ?>
			<?php echo $form->radioButtonList($model,'jk',array('L'=>'Laki-Laki','P'=>'Perempuan')); ?>
		</div>
		<?php echo $form->error($model,'jk'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->