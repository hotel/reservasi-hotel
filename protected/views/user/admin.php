<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-pencil"></i> List Data User <span class="badge badge-success pull-right"> + </span>', 'url'=>array('index')),
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data User <span class="badge badge-success pull-right"> + </span>', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h1>Kelola User</h1>
<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		array(
      		'header'=>'No',
        	'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
        ),
		array(
			'name'=>'id_level',
			'value'=>function($data){
				return $data->Level->level;
			}
		),
		'username',
		'password',
		//'saltPassword',
		'nama_lengkap',
		'keterangan',
		'email',
		//'join_date',
		//'avatar',
         array(
                 'class'=>'JToggleColumn',
                 'name'=>'status', // boolean model attribute (tinyint(1) with values 0 or 1)
                  //'filter' => array('0' => 'Ditolak', '1' => 'Disetujui'), // filter
                  //'action'=>'switch', // other action, default is 'toggle' action
                  'checkedButtonLabel'=>'images/togle/1.png',  // Image,text-label or Html
                  'uncheckedButtonLabel'=>'images/togle/0.png', // Image,text-label or Html
                  'checkedButtonTitle'=>'Diterima', // tooltip
                  'uncheckedButtonTitle'=>'Dipesan', // tooltip
                  'labeltype'=>'image',// New Option - may be 'image','html' or 'text'
                   'htmlOptions'=>array('style'=>'text-align:center;min-width:60px;')
          ),		
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
