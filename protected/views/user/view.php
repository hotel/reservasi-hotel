<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Data User <span class="badge badge-success pull-right"> + </span>', 'url'=>array('index')),
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data User <span class="badge badge-success pull-right"> + </span>', 'url'=>array('create')),
	array('label'=>'<i class="icon icon-pencil"></i> Ubah Data User <span class="badge badge-success pull-right"> + </span>', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'<i class="icon icon-trash"></i> Hapus Data User <span class="badge badge-success pull-right"> + </span>', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=>Yii::app()->user->getLevel()==1),
	array('label'=>'<i class="icon icon-cog"></i> Kelola User <span class="badge badge-success pull-right"> + </span>', 'url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>

<h1>View User #<?php echo $model->id; ?></h1>
<?/*
	foreach(Yii::app()->user->getFlashes() as $key => $message){
		echo '<div class="flash-'.$key.'">'.$message."</div>\n";
	}*/
?>
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'Level.level',
		'username',
		'password',
		//'saltPassword',
		'nama_lengkap',
		'keterangan',
		'email',
		'join_date',
		array(
			'label'=>'Avatar',
			'type'=>'raw',
			'value'=>CHtml::image('a/../Avatar/'.$model->avatar,'no-image', array("width"=>100)),
		),
		//'status',
	),
)); ?>
