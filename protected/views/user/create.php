<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Data User <span class="badge badge-success pull-right"> + </span>', 'url'=>array('index')),
	array('label'=>'<i class="icon icon-cog"></i> Kelola User <span class="badge badge-success pull-right"> + </span>', 'url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>
<div class="form-actions">
	<h1>Tambah User</h1>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>