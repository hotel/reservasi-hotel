<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-pencil"></i> List Data User <span class="badge badge-success pull-right"> + </span>', 'url'=>array('index')),
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data User <span class="badge badge-success pull-right"> + </span>', 'url'=>array('create')),
	array('label'=>'<i class="icon icon-tag"></i> Lihat Data User <span class="badge badge-success pull-right"> + </span>', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'<i class="icon icon-cog"></i> Kelola User <span class="badge badge-success pull-right"> + </span>', 'url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>

<h1>Update User <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>