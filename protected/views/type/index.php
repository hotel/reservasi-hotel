<?php
/* @var $this TypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Types',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data Class <span class="badge badge-success pull-right"> + </span>', 'url'=>array('create'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-cog"></i> Kelola Class <span class="badge badge-success pull-right"> + </span>', 'url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>

<h1>Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
