<?php
/* @var $this TypeController */
/* @var $model Type */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'type-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'class'); ?>
		<?php echo $form->textField($model,'class',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'class'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'harga'); ?>
		<?php echo $form->textField($model,'harga',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'harga'); ?>
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array('type'=>'primary', 'label'=>'Simpan','buttonType'=>'submit')); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array('type'=>'reset', 'label'=>'Batal','buttonType'=>'reset')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->