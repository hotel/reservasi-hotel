<?php
/* @var $this TypeController */
/* @var $model Type */

$this->breadcrumbs=array(
	'Types'=>array('index'),
	$model->idclass,
);

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Data Class <span class="badge badge-success pull-right"> + </span>', 'url'=>array('index'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data Class <span class="badge badge-success pull-right"> + </span>', 'url'=>array('create'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-pencil"></i> Ubah Data Class <span class="badge badge-success pull-right"> + </span>', 'url'=>array('update', 'id'=>$model->idclass),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-trash"></i> Hapus Data Class <span class="badge badge-success pull-right"> + </span>', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idclass),'confirm'=>'Are you sure you want to delete this item?'),'visible'=>Yii::app()->user->getLevel()==1),
	array('label'=>'<i class="icon icon-cog"></i> Kelola Class <span class="badge badge-success pull-right"> + </span>', 'url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>

<h1>View Type #<?php echo $model->idclass; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idclass',
		'class',
		'harga',
	),
)); ?>
