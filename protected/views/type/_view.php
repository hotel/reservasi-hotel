<?php
/* @var $this TypeController */
/* @var $data Type */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idclass')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idclass), array('view', 'id'=>$data->idclass)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('class')); ?>:</b>
	<?php echo CHtml::encode($data->class); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('harga')); ?>:</b>
	<?php echo CHtml::encode($data->harga); ?>
	<br />
</div>