<?php
/* @var $this TypeController */
/* @var $model Type */

$this->breadcrumbs=array(
	'Types'=>array('index'),
	$model->idclass=>array('view','id'=>$model->idclass),
	'Update',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Data Class <span class="badge badge-success pull-right"> + </span>', 'url'=>array('index'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data Class <span class="badge badge-success pull-right"> + </span>', 'url'=>array('create'),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-search"></i> Lihat Data Class <span class="badge badge-success pull-right"> + </span>', 'url'=>array('view', 'id'=>$model->idclass),'visible'=>!yii::app()->user->isGuest),
	array('label'=>'<i class="icon icon-cog"></i> Kelola Class <span class="badge badge-success pull-right"> + </span>', 'url'=>array('admin'),'visible'=>Yii::app()->user->getLevel()==1),
);
?>

<h1>Update Type <?php echo $model->idclass; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>