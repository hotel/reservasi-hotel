<?php
/* @var $this BeritaController */
/* @var $model Berita */

$this->breadcrumbs=array(
	'Beritas'=>array('index'),
	$model->title=>array('view','id'=>$model->id_berita),
	'Update',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Berita', 'url'=>array('index')),
	array('label'=>'<i class="icon icon-pencil"></i> Create Berita', 'url'=>array('create')),
	array('label'=>'<i class="icon icon-search"></i> View Berita', 'url'=>array('view', 'id'=>$model->id_berita)),
	array('label'=>'<i class="icon icon-cog"></i> Manage Berita', 'url'=>array('admin')),
);
?>

<h1>Update Berita <?php echo $model->id_berita; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>