<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id_admin=>array('view','id'=>$model->id_admin),
	'Update',
);

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Data User', 'url'=>array('index')),
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data User', 'url'=>array('create')),
	array('label'=>'<i class="icon icon-search"></i> Tampil Data User', 'url'=>array('view', 'id'=>$model->id_admin)),
	array('label'=>'<i class="icon icon-cog"></i> Kelola User', 'url'=>array('admin')),
);
?>

<h1>Update User <?php echo $model->id_admin; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>