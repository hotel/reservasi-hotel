<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id_admin,
);

$this->menu=array(
	array('label'=>'<i class="icon icon-list"></i> List Data User', 'url'=>array('index')),
	array('label'=>'<i class="icon icon-pencil"></i> Tambah Data User', 'url'=>array('create')),
	array('label'=>'<i class="icon icon-pencil"></i> Ubah Data User', 'url'=>array('update', 'id'=>$model->id_admin)),
	array('label'=>'<i class="icon icon-trash"></i> Hapus Data User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_admin),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'<i class="icon icon-cog"></i> Kelola User', 'url'=>array('admin')),
);
?>

<h1>View User #<?php echo $model->id_admin; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_admin',
		'id_level',
		'username',
		'password',
		'nama_lengkap',
		'keterangan',
		'email',
		'join_date',
		array(
			'label'=>'Avatar',
						'type'=>'raw',
			'value'=>CHtml::image('a/../Avatar/'.$model->avatar,'DORE', array("width"=>100)),
		),
		'status',
	),
)); ?>
