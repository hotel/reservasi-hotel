<?php

require('../plugins/fpdf17/fpdf.php');
require('../set/koneksidb.php');

class PDF extends FPDF
{
function Header() {
$this->Image('hotel.png',10,6,25);

//$this->Image('fajar.png',170,6,30);

    $this->SetFont('Arial','B',14);

    $this->Cell(0,10,' MY H.O.T.E.L ',0,0,'C');
 
    $this->Ln();
 
    $this->SetFont('Arial','B',14);
 
    $this->Cell(0,10,'Data Reservasi',0,0,'C');
 
    $this->Ln();
 
    $this->SetFont('Arial','',9);
 
    $this->Cell(0,3,'Jl. Soekarno Hatta No.456 Telp (022)666333',0,0,'C');
 
    $this->Ln();
 
    $this->Ln();
	
    $this->SetFont('Arial','',20);
    $this->Line(10, 34, 200, 34);
 
  
}
 
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-13);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
	
    $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}
// Load data
function LoadData($file)
{
	// Read file lines
	$lines = file($file);
	$data = array();
	foreach($lines as $line)
		$data[] = explode(';',trim($line));
	return $data;
}
function LoadDataFromSQL($sql)
{
	$hasil=mysql_query($sql) or die(mysql_error());

	$data = array();
	while($rows=mysql_fetch_array($hasil)){
		$data[] = $rows;

}
	return $data;
}


// Colored table
function FancyTable($header, $data)
{
	// Colors, line width and bold font
	$this->SetFillColor(255,1,74);
	$this->SetTextColor(255);
	$this->SetDrawColor(128,0,0);
	$this->SetLineWidth(.3);
	$this->SetFont('','B');
	// Header
	$w = array( 30, 46, 27, 27, 30, 30);
	for($i=0;$i<count($header);$i++)
		$this->Cell($w[$i],7,$header[$i],1,0,'C',true);
	$this->Ln();
	// Color and font restoration
	$this->SetFillColor(224,235,255);
	$this->SetTextColor(0);
	$this->SetFont('');
	// Data
	$fill = false;

	foreach($data as $row)
	{
	
		$this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
		$this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
		$this->Cell($w[2],6,$row[2],'LR',0,'R',$fill);
		$this->Cell($w[3],6,$row[3],'LR',0,'R',$fill);
		//$this->Cell($w[4],6,$row[4],'LR',0,'R',$fill);
		$this->Cell($w[5],6,$row[5],'LR',0,'R',$fill);
		$this->Cell($w[6],6,$row[6],'LR',0,'R',$fill);
		
		$this->Ln();
		$fill = !$fill;
	
	}
	// Closing line
	$this->Cell(array_sum($w),0,'','T');
}
}

$pdf = new PDF();
// Column headings
$header = array('Nama', 'email', 'check-in', 'check-out','Lama Inap', 'Total');
// Data loading

$query="select nama_pemesan,email,check_in,check_out,telepon,lama_inap,total from pesan";
 
$data = $pdf->LoadDataFromSQL($query);
$pdf->SetFont('Arial','',11);
$pdf->AddPage();

$pdf->FancyTable($header,$data);
$pdf->Output();
?>
