	<footer>
        <div class="subnav navbar navbar-fixed-bottom">
            <div class="navbar-inner" style="background:#fff">
                <div class="container">
                    Designed by <a href="http://www.webapplicationthemes.com" target="_new">webapplicationthemes.com</a>. All Rights Reserved.<br /><small>Powered by <a href="http://www.yiiframework.com" title="Yii Framework" target="_new">Yii Framework</a> and <a href="http://twitter.github.com/bootstrap/" title="Twitter Bootstrap" target="_new">Twitter Bootstrap</a></small>
       				<img align="right" src="<?php echo Yii::app()->theme->baseUrl ?>/img/facebook_32.png" title="facebook">      
					<img align="right" src="<?php echo Yii::app()->theme->baseUrl ?>/img/twitter_32.png" title="twitter">
					<img align="right" src="<?php echo Yii::app()->theme->baseUrl ?>/img/youtube_32.png" title="youtube">				
				</div>
            </div>
        </div>      
	</footer>

