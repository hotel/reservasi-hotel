<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
    <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
     
          <!-- Be sure to leave the brand out there if you want it shown -->
          <a class="brand" href="#">My Hotel</a>
          
          <div class="nav-collapse">
			<?php $this->widget('zii.widgets.CMenu',array(
                    'htmlOptions'=>array('class'=>'pull-right nav'),
                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
					'itemCssClass'=>'item-test',
                    'encodeLabel'=>false,
                    'items'=>array(
                        array('label'=>'<i class="icon icon-home"></i>Home', 'url'=>array('/site/index'),'visible'=>Yii::app()->user->isGuest),
                        array('label'=>'<i class="icon icon-tags"></i>News', 'url'=>array('/news/'),'visible'=>Yii::app()->user->isGuest),
                        array('label'=>'<i class="icon icon-tags"></i>Galery', 'url'=>array('/Kamar/'),'visible'=>Yii::app()->user->isGuest),												
                        /*array('label'=>'Gii generated', 'url'=>array('customer/index')),*/
							array('label'=>'<i class="icon icon-tags"></i> Reservasi', 'url'=>array('/pesan/create'),'visible'=>Yii::app()->user->isGuest),
							array('label'=>'<i class="icon icon-tags"></i> Contact', 'url'=>array('/site/contact'),'visible'=>Yii::app()->user->isGuest),
							array('label'=>'<i class="icon icon-home"></i> Berita', 'url'=>array('/news/'), 'visible'=>!Yii::app()->user->isGuest),
							array('label'=>'<i class="icon icon-tags"></i> Room', 'url'=>array('/Kamar/index'), 'visible'=>!Yii::app()->user->isGuest),
							array('label'=>'<i class="icon icon-tags"></i> Booking', 'url'=>array('/pesan/'), 'visible'=>!Yii::app()->user->isGuest),
							array('label'=>'<i class="icon icon-tags"></i> Type', 'url'=>array('/type/'), 'visible'=>!Yii::app()->user->isGuest),

				array('label'=>'<i class="icon icon-file"></i> Laporan<span class="caret"></span>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                        'items'=>array(
							array('label'=>'<i class="icon icon-file"></"></i> Reservasi', 'url'=>"pdf",'visible'=>Yii::app()->user->getLevel()==1),
							),'visible'=>Yii::app()->user->getLevel()==1),							
							
							array('label'=>'<i class="icon icon-tags"></i> User', 'url'=>array('/user/'), 'visible'=>!Yii::app()->user->isGuest),											    
							array('label'=>'<i class="icon icon-user"></i> My Account <span class="caret"></span>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                        'items'=>array(
						array('label'=>'<i class="icon icon-hand-right"></i> Ubah Data ', 'url'=>array('/user/update','id'=>Yii::app()->user->id), 'visible'=>!Yii::app()->user->isGuest),
						array('label'=>'<i class="icon icon-off"></"></i> Keluar ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
							),'visible'=>!Yii::app()->user->isGuest),
                        array('label'=>'<i class="icon icon-lock"></i> Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),

                    ),
                )); ?>
    	</div>
    </div>
	</div>
</div>

<div class="subnav navbar navbar-fixed-top">
    <div class="navbar-inner">
    	<div class="container">
        	<div class="style-switcher pull-left">
              
          	</div>

    	</div><!-- container -->
    </div><!-- navbar-inner -->
</div><!-- subnav -->