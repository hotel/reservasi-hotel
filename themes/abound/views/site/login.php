<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm */ 

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<?php /*
<div class="page-header">
	<h1>Login <small>to your account</small></h1>
</div>*/?>
<?php /*<div class="row-fluid">*/?>
	
<?php /*<div class="span6" style="border:3px 3px 3px 3px;">*/?>
<?php/*
	$this->beginWidget('zii.widgets.CPortlet', array(
		'title'=>"Private access",
	));
	*/
?>
<h2>Login</h2>    
    <div class="form">
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>true,
		'type' => 'horizontal',
        'htmlOptions' => array('class' => 'well'),
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>
        
        <div class="row">
            <?php echo $form->labelEx($model,'username'); ?>
            <?php echo $form->textField($model,'username'); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'password'); ?>
            <?php echo $form->passwordField($model,'password'); ?>
            <?php echo $form->error($model,'password'); ?>
            <p class="hint">

            </p>
        </div>
    
        <div class="row rememberMe">
            <?php echo $form->checkBox($model,'rememberMe'); ?>
            <?php echo $form->label($model,'rememberMe'); ?>
            <?php echo $form->error($model,'rememberMe'); ?>
        </div>
    
        <div class="row buttons">
            <?php echo CHtml::submitButton('Login',array('class'=>'btn btn btn-primary')); ?>
        </div>
    
    <?php $this->endWidget(); ?>
    </div><!-- form -->

<?php /*$this->endWidget();*/?>
<?php /*</div>*/?>
<?php /*</div>*/ ?>