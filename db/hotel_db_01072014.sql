-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 01, 2014 at 09:34 AM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hotel_db`
--
CREATE DATABASE `hotel_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `hotel_db`;

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `foto` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_berita`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `berita`
--


-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `idclass` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `harga` varchar(20) NOT NULL,
  PRIMARY KEY (`idclass`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`idclass`, `class`, `harga`) VALUES
(1, 'Single Room', '250000'),
(2, 'Double Room', '300000'),
(3, 'Twin Room', '400000'),
(4, 'Suite Room', '500000'),
(5, 'asdas', '123123');

-- --------------------------------------------------------

--
-- Table structure for table `dr_category`
--

CREATE TABLE IF NOT EXISTS `dr_category` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `parent` int(4) NOT NULL,
  `category` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `dr_category`
--

INSERT INTO `dr_category` (`id`, `parent`, `category`) VALUES
(1, 0, 'News Category'),
(2, 1, 'News'),
(3, 1, 'Event'),
(4, 0, 'Album Category'),
(5, 4, 'Event'),
(6, 4, 'Member'),
(7, 0, 'Jenis Mobil'),
(8, 7, 'Ep 70'),
(9, 7, 'Ep 71'),
(10, 7, 'Ep 80'),
(11, 7, 'Ep 81'),
(12, 7, 'Ep 82'),
(13, 7, 'Ep 83');

-- --------------------------------------------------------

--
-- Table structure for table `dr_news`
--

CREATE TABLE IF NOT EXISTS `dr_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(4) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `description` text NOT NULL,
  `datenews` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `dr_news`
--

INSERT INTO `dr_news` (`id`, `category_id`, `title`, `slug`, `image`, `description`, `datenews`, `published`) VALUES
(6, 2, 'qwert', 'qwert', '2014-06-28-45464_1375715495.jpg', '<p>asdasd</p>', '2014-06-01 00:00:00', 1),
(7, 2, 'PunkPinkPunk', 'punkpinkpunk', '2014-06-28-2.jpg', '<p>PunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunk</p>\r\n<p>PunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunk</p>\r\n<p>&nbsp;</p>\r\n<p>PunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunk</p>\r\n<p>PunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunk </p>\r\n<p>PunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunk</p>\r\n<p>PunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunk</p>\r\n<p>PunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunk</p>\r\n<p>PunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunk</p>\r\n<p>PunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunk</p>\r\n<p>PunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunk</p>\r\n<p>PunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunk</p>\r\n<p>PunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunkPunkPinkPunk</p>', '2014-06-28 00:00:00', 1),
(4, 2, 'qwerty', 'qwerty', '2014-06-16-3d-abstract-shapes.jpg', '<p>hagsdjfhgasjdhfgsaa</p>', '2014-06-16 00:00:00', 1),
(5, 2, 'qwerty', 'qwerty', '2014-06-17-3d-abstract-shapes.jpg', '	<p>hagsdjfhgasjdhfgsaa</p> ', '2014-06-17 00:00:00', 1),
(8, 2, 'Ada Kabar Anak Hilang', 'ada-kabar-anak-hilang', '2014-06-28-avatar.jpg', '<p>Itu adalah sosok orang hilang</p>', '2014-06-29 00:00:00', 1),
(9, 2, 'Pertarungan Bola', 'pertarungan-bola', '2014-06-28-3_kesalahan_menabung_1375715639.jpg', '<p>pertarungan antara cwo dengan cwo !!!</p>', '2014-06-28 00:00:00', 1),
(10, 2, 'teterwr', 'teterwr', NULL, '<p>hgfhghgfh</p>', '2014-06-28 00:00:00', 1),
(11, 2, 'Panjat Pinang', 'panjat-pinang', '2014-06-28-628x471_1375722344.jpg', '<p>Panjat pinang</p>', '2014-06-28 23:28:48', 1),
(12, 2, 'Kejadian Misteri Dalam celana', 'kejadian-misteri-dalam-celana', '2014-06-28-suiteroom_1387360588.jpg', '<p>quytweuqytwuqytewuqywteuqytweuqyw</p>', '2014-06-28 23:39:29', 1),
(13, 3, 'Balap Kerupuk Antar RT', 'balap-kerupuk-antar-rt', '2014-06-28-istanabogor_1387379433.jpg', '<p>Memenangkan Hadiah wkwkwkkwk</p>', '2014-06-28 23:45:32', 1),
(14, 3, 'Maraton', 'maraton', '2014-06-28-computer-virus-character-hi_1375715930.png', '<p>wkwkwkwkwkkkkkkkkkkkkkkkkkkkkkkkkkwwwwwwwwwwwwwwwwwwwwwwwkkkkkkkkkkk</p>', '2014-06-28 23:48:45', 1),
(15, 2, 'buy 1 get 1', 'buy-1-get-1', '2014-07-01-images11.jpg', '<p>enak uyyyy....</p>', '2014-07-01 08:47:56', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kamar`
--

CREATE TABLE IF NOT EXISTS `kamar` (
  `id_kamar` int(11) NOT NULL AUTO_INCREMENT,
  `id_class` int(11) NOT NULL,
  `namakamar` varchar(15) NOT NULL,
  `keterangan` text,
  `gambar` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_kamar`),
  KEY `id_class` (`id_class`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `kamar`
--

INSERT INTO `kamar` (`id_kamar`, `id_class`, `namakamar`, `keterangan`, `gambar`) VALUES
(1, 1, 'AL-1', '<p>Kamar yang sangat bagus</p>', '2014-06-28-presidentialsuite.jpg'),
(2, 1, 'AL-1', '<p>Kamar yang sangat bagus</p>', '2014-06-28-2.jpg'),
(3, 2, 'Sasae', '<p>aeiwqwqwq</p>', '2014-06-28-7.jpg'),
(4, 4, 'BaseMan', '<p>Baso</p>', '2014-06-28-doubleroom_1387360011.jpg'),
(5, 3, 'qwewqeqw', '<p>wqweqeqw</p>', '2014-06-28-index.jpg'),
(6, 4, 'Aselole', '<p>Hotel megah</p>', '2014-06-28-istanabogor.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `id_level` int(2) NOT NULL AUTO_INCREMENT,
  `level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `level`) VALUES
(1, 'Super Admin'),
(2, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE IF NOT EXISTS `pesan` (
  `id_pesan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pemesan` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `check_in` date NOT NULL,
  `check_out` date NOT NULL,
  `lama_inap` varchar(20) DEFAULT NULL,
  `id_class` int(11) NOT NULL,
  `harga` varchar(15) NOT NULL,
  `id_kamar` int(11) NOT NULL,
  `total` varchar(15) DEFAULT NULL,
  `status` varchar(20) DEFAULT '0',
  PRIMARY KEY (`id_pesan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id_pesan`, `nama_pemesan`, `email`, `telepon`, `check_in`, `check_out`, `lama_inap`, `id_class`, `harga`, `id_kamar`, `total`, `status`) VALUES
(7, 'asd', '', NULL, '2014-06-24', '2014-06-25', '1 Hari', 1, '1', 1, '250,000', '1'),
(8, 'tasut', '', NULL, '2014-06-26', '2014-06-28', '2 Hari', 2, '2', 1, '500,000', '1'),
(9, 'sah', '', NULL, '2014-06-26', '2014-06-27', NULL, 2, '2', 1, '0', '1'),
(10, 'jj', '', NULL, '2014-06-26', '2014-06-28', NULL, 2, '2', 1, '0', '1'),
(11, 'shadj', '', NULL, '2014-06-26', '2014-06-28', NULL, 1, '1', 1, '600000', '1'),
(12, 'hdfhj', '', NULL, '2014-06-26', '2014-06-30', NULL, 4, '4', 1, '2000000', '1'),
(13, 'jdskj', '', NULL, '2014-06-26', '2014-06-28', NULL, 4, '4', 1, '1,000,000', 'dipesan'),
(14, 'tataw', '', NULL, '2014-06-26', '2014-06-29', NULL, 4, '4', 1, '300,000', 'dipesan'),
(15, 'tataw', '', NULL, '2014-06-27', '2014-06-28', NULL, 2, '2', 1, '300,000', 'dipesan'),
(16, 'tata', '', NULL, '2014-06-27', '2014-06-30', NULL, 2, '2', 1, '900,000', 'dipesan'),
(17, 'tatatatatta', '', NULL, '2014-06-27', '2014-06-30', '3', 2, '2', 1, '900,000', 'dipesan'),
(20, 'www', '', NULL, '2014-06-27', '2014-06-29', '2 Hari', 2, '2', 1, '600,000', 'dipesan'),
(21, 'qweeqwe', '', NULL, '2014-06-27', '2014-06-28', '1 Hari', 1, '1', 1, '250,000', 'dipesan'),
(22, 'asep', '', NULL, '2014-06-27', '2014-06-28', '0 Hari', 3, '3', 1, '0', 'dipesan'),
(23, 'intan', '', NULL, '2014-06-27', '2014-06-29', '2 Hari', 1, '1', 1, '500,000', 'dipesan'),
(24, 'qwe', '', NULL, '2014-06-27', '2014-06-28', '1 Hari', 4, '4', 1, 'Rp.500,000', 'dipesan'),
(25, 'jono', '', NULL, '2014-06-27', '2014-06-28', '1 Hari', 2, '2', 2, 'Rp.300,000', 'dipesan'),
(26, 'Leho', '', NULL, '2014-06-28', '2014-06-30', '2 Hari', 4, '4', 2, 'Rp.1,000,000', 'dipesan'),
(27, 'diah', '', NULL, '2014-06-28', '2014-07-02', '4 Hari', 1, '1', 3, 'Rp.1,000,000', 'dipesan'),
(28, 'hasem', '', NULL, '2014-06-28', '2014-06-30', '2 Hari', 4, '4', 1, 'Rp.1,000,000', 'dipesan'),
(29, 'Marsela', '', NULL, '2014-06-28', '2014-06-29', '1 Hari', 4, '4', 4, 'Rp.500,000', '0'),
(31, 'Galih', '', NULL, '2014-06-29', '2014-06-30', '1 Hari', 4, '4', 1, 'Rp.500,000', '0'),
(32, 'Gilang', '', NULL, '2014-06-29', '2014-06-30', '1 Hari', 4, '4', 1, 'Rp.500,000', '0'),
(33, 'Bowo', '', NULL, '2014-06-29', '2014-06-30', '1 Hari', 3, '3', 1, 'Rp.400,000', '0'),
(34, 'qweqwe', '', NULL, '2014-06-29', '2014-06-30', '1 Hari', 4, '4', 2, 'Rp.500,000', '0'),
(35, 'chaw', '', NULL, '2014-06-29', '2014-06-30', '1 Hari', 2, '2', 1, 'Rp.300,000', '0'),
(37, 'thasut', 'tatasutiadi@gmail.com', '08997654', '2014-06-30', '2014-07-01', '1 Hari', 3, '3', 1, 'Rp.400,000', '0'),
(38, 'tata', 'tatasutiadi@gmail.com', '08997654', '2014-06-30', '2014-07-01', '1 Hari', 4, '', 1, 'Rp.500,000', '0'),
(39, 'tata', 'tatasutiadi@gmail.com', '08997654', '2014-06-30', '2014-07-01', '1 Hari', 4, '', 1, 'Rp.500,000', '0'),
(40, 'tata', 'tatasutiadi@gmail.com', '08997654', '2014-06-30', '2014-07-01', '1 Hari', 4, '', 1, 'Rp.500,000', '0'),
(41, 'tata', 'tatasutiadi@gmail.com', '08997654', '2014-06-30', '2014-07-01', '1 Hari', 4, '', 1, 'Rp.500,000', '0'),
(42, 'tata', 'tatasutiadi@gmail.com', '', '2014-06-30', '2014-07-01', '1 Hari', 4, '4', 2, 'Rp.500,000', '0'),
(43, 'Tata', 'tatasutiadi@gmail.com', '08997654', '2014-06-30', '2014-07-01', '1 Hari', 2, '2', 1, 'Rp.300,000', '0'),
(44, 'Tata Sutiadi', 'tatasutiadi@gmail.com', '085794799556', '2014-06-30', '2014-07-02', '2 Hari', 4, '4', 3, 'Rp.1,000,000', '1'),
(45, 'Wiliam', 'william@gmail.com', '', '2014-06-30', '2014-07-01', '1 Hari', 4, '4', 1, 'Rp.500,000', '0'),
(46, 'Asep Mulyadi', 'asep@gmail.com', '', '2014-07-01', '2014-07-03', '2 Hari', 4, '4', 2, 'Rp.1,000,000', '0');

-- --------------------------------------------------------

--
-- Table structure for table `registrasi`
--

CREATE TABLE IF NOT EXISTS `registrasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ktp` varchar(30) NOT NULL,
  `id_pesan` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(13) NOT NULL,
  `email` varchar(30) NOT NULL,
  `jk` enum('L','P') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ktp` (`ktp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `registrasi`
--

INSERT INTO `registrasi` (`id`, `ktp`, `id_pesan`, `alamat`, `telepon`, `email`, `jk`) VALUES
(1, '12345', '', '', '213781638', 'tata@gmail.com', ''),
(2, '12133141', '', '', '2312313121', '123@gmail.com', ''),
(3, '098', '7', '', '567576', 'tatatata@gmail.com', ''),
(4, '78767', '', '', '7657', '465@gmail.com', 'L'),
(30, '76253476253', '', '', '83695467458', 'utasdfu@gmail.com', 'L'),
(32, '7234826732', '', '', '7326486274', 'jghsfja@kjhg.com', 'P'),
(33, '987694', '', '', '86544354', 'tatatatat@gmail.com', 'L'),
(34, '82376482', '12', '', '56546565', 'ahsgdfjah@mailcom', 'L'),
(35, '23423423', '', '', '27348237', 'jhagdjhgas@mail.com', 'L'),
(36, '723648', '25', '', '87865765', 'agfjhgaj@mail.com', 'L'),
(37, '09456704598', '', '', '879234827', 'kjsdhfkjsd@mail.com', ''),
(38, '345345345', '', '', '675765765', 'jashjhg@mail.com', ''),
(39, '7863862784', '', '', '862362472', 'jasdhgjasdhg@mailcom', ''),
(40, '565465', '', '', '8763485237', 'kahdfkasjhd', 'L'),
(41, '123456', '', '', '089976543', 'sutiadi@gmail.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_level` int(2) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `saltPassword` varchar(50) NOT NULL,
  `nama_lengkap` varchar(100) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `join_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `avatar` varchar(30) DEFAULT NULL,
  `status` varchar(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `id_level`, `username`, `password`, `saltPassword`, `nama_lengkap`, `keterangan`, `email`, `join_date`, `avatar`, `status`) VALUES
(1, 1, 'tata', '21232f297a57a5a743894a0e4a801fc3', '', 'Tata Sutiadi', '-', 'tatasutiadi@gmail.com', '2014-06-12 23:08:48', NULL, '1'),
(2, 1, 'intan_marsela', '9dad7e29353b87b61a8ebf4245b186fb', '53b150420435e9.69920658', '', '', '', '2014-06-30 19:55:22', NULL, '1'),
(3, 2, 'asep', '79a8e2353d3a98ea1abc651733d2c180', '53b15078e4d109.64586296', '', '', '', '2014-06-30 19:56:18', '2014-06-30-url.jpg', '1'),
(4, 1, 'Tataw', 'ddc57e27f3531e14f3b57c48f7c6fc6c', '53b15e3e92a6d6.61142844', 'Tata Sutiadi', '', '', '2014-06-30 20:55:04', '2014-06-30-20130823_160231.jpg', '1'),
(8, 2, 'Wiliam', 'c995529883da9b362166e972ddddc697', '53b164788af9e1.35353904', '', '', '', '2014-06-30 21:21:36', NULL, '0');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
